from unittest import TestCase
from book_classwork11 import\
    Book, AuthorNameError, YearError, PageNumberError, PriceError
from unittest import mock


def mock_validate_author_name(name):
    return name


class TestBook(TestCase):
    def test_validate_author_name_ok(self):
        self.book = Book('Author', 2000, 1000, 500)

    def test_validate_author_name_digit(self):
        with self.assertRaises(AuthorNameError) as error:
            self.book = Book('1Author', 2000, 1000, 500)
        self.assertEqual(
            str(error.exception), 'Author name should have only letters')

    def test_validate_author_name_special_chars(self):
        with self.assertRaises(AuthorNameError) as error:
            self.book = Book('Author-Name', 2000, 1000, 500)
        self.assertEqual(
            str(error.exception), 'Author name should have only letters')

    def test_validate_year_ok(self):
        self.book = Book('Author', 1980, 1000, 500)

    def test_validate_year_too_early(self):
        with self.assertRaises(YearError) as error:
            self.book = Book('Author', 1979, 1000, 500)
        self.assertEqual(
            str(error.exception), 'Year should not be less then 1980')

    def test_validate_pages_ok(self):
        self.book = Book('Author', 2020, 4000, 500)

    def test_validate_pages_too_many(self):
        with self.assertRaises(PageNumberError) as error:
            self.book = Book('Author', 2020, 4001, 500)
        self.assertEqual(
            str(error.exception), 'Page number should not be more then 4000')

    def test_validate_price_ok(self):
        self.book1 = Book('Author', 2020, 10, 100)
        self.book2 = Book('Author', 2020, 10, 10000)

# другой вариант записи
    def test_validate_price_too_low(self):
        with self.assertRaises(PriceError) as error:
            self.book = Book('Author', 2020, 200, 99.99)
        self.assertEqual(
            error.exception.message, 'Price should be between 100 and 10000')

    def test_validate_price_too_high(self):
        with self.assertRaises(PriceError) as error:
            self.book = Book('Author', 2020, 200, 10000.01)
        self.assertEqual(
            str(error.exception), 'Price should be between 100 and 10000')

    @mock.patch('book_classwork11.Book.validate_author_name',
                side_effect=mock_validate_author_name)
    def test_validate_author_name_digit_2(self, validate_author_name):
        self.book = Book('Author2', 2000, 1000, 500)
        self.assertEqual(validate_author_name('Author2'), 'Author2')
