from pages.base_page import BasePage
from pages.main_page_locators import MainPageLocators
from pages.login_or_register_page import LoginOrRegisterPage


class MainPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_login_page(self):
        login_page = self.driver.find_element(
            *MainPageLocators.LOGIN_OR_REGISTER_LINK)
        login_page.click()
        return LoginOrRegisterPage(self.driver)

    def open_offers(self):
        offers = self.driver.find_element(*MainPageLocators.OFFERS_LINK)
        offers.click()

    def open_books_page(self):
        books_page = self.driver.find_element(*MainPageLocators.BOOKS_LINK)
        books_page.click()
