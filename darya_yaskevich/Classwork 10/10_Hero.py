from random import choice


class Hero:
    hero_counter = 0

    def __init__(self, team=None):
        Hero.hero_counter += 1
        self.hero_id = f'hero{Hero.hero_counter}'
        self.team = team
        self.level = 1

    def level_up(self):
        self.level += 1
        print(f'{self.hero_id} leveled up!')


class Soldier:
    soldier_counter = 0

    def __init__(self, team=None):
        Soldier.soldier_counter += 1
        self.soldier_id = f'soldier{Soldier.soldier_counter}'
        self.team = team

    def move_to_hero(self, hero):
        print(f'{self.soldier_id} followed {hero.hero_id}')


class Team:
    def __init__(self, name=None, hero=None):
        self.name = name
        self.hero = hero
        self.soldiers = []

    def soldier_count(self):
        return len(self.soldiers)


def create_soldiers_with_teams(playing_teams, n):
    for i in range(n):
        team = choice(playing_teams)
        soldier = Soldier(team=team.name)
        team.soldiers.append(soldier)


if __name__ == '__main__':
    hero1 = Hero(team='crusaders')
    hero2 = Hero(team='samurai')

    team1 = Team('crusaders', hero=hero1)
    team2 = Team('samurai', hero=hero2)
    teams = [team1, team2]

    # создаем 30 солдат
    create_soldiers_with_teams(teams, 30)

    print(f'team1: {team1.soldier_count()} soldiers')
    print(f'team2: {team2.soldier_count()} soldiers')

    # повышаем уровень героя команды, у которой больше солдат
    if team1.soldier_count() > team2.soldier_count():
        team1.hero.level_up()
    else:
        team2.hero.level_up()

    # Отправить одного из солдат первого героя следовать за ним
    chosen_soldier = choice(team1.soldiers)
    chosen_soldier.move_to_hero(team1.hero)
    # Вывести на экран идентификационные номера этих двух юнитов
    print(team1.hero.hero_id, chosen_soldier.soldier_id)
