class Flower:
    def __init__(self, freshness, color, stem_length):
        self.freshness = freshness
        self.color = color
        self.stem_length = stem_length

    def __str__(self):
        return f'{self.color} {self.__class__.__name__}'


class Rose(Flower):
    price = 15.00
    freshness_period = 7


class Tulip(Flower):
    price = 3.50
    freshness_period = 10


class Violet(Flower):
    price = 10.00
    freshness_period = 3


class Chamomile(Flower):
    price = 21.00
    freshness_period = 10


class Wrapping:
    def __init__(self, material, color, price: float):
        self.material = material
        self.color = color
        self.price = price


class Ribbon:
    def __init__(self, material, color, price: float):
        self.material = material
        self.color = color
        self.price = price


class Bouquet:
    def __init__(self, wrapping=None, ribbon=None, *flowers):
        self.wrapping = wrapping
        self.ribbon = ribbon
        self.flowers = flowers
        self.price = sum([flower.price for flower in self.flowers])\
            + self.ribbon.price + self.wrapping.price

    def __str__(self):
        return f'Bouquet: {self.flowers}, {self.wrapping}, {self.ribbon}'

    def __contains__(self, item):
        return item in self.flowers

    def __getitem__(self, index):
        return self.flowers[index]

    def __iter__(self):
        return iter(self.flowers)

    @property
    def get_price(self):
        return f'${self.price:.2f}'

    def wilt_time(self):
        """Метод определяет время увядания по среднему времени жизни всех
         цветов в букете"""
        freshness_all = [flower.freshness_period for flower in self.flowers]
        average_time = sum(freshness_all) // len(freshness_all)
        return f'The bouquet will last for around {average_time} days'

    def search(self, word):
        search_results = [i.__str__() for i in self.flowers if
                          word in i.color or
                          word in i.__class__.__name__.lower()]
        if search_results:
            return search_results
        else:
            return 'No results found'

    def sort_flowers(self, feature: str):
        """
        Сортировка цветов  в букете на основе параметра (цена, цвет и тд)
        :param feature: name of the flower parameter
        :return: list of flowers in ascending order
        """
        criteria = {
            'color': lambda flower: flower.color,
            'freshness': lambda flower: flower.freshness,
            'stem_length': lambda flower: flower.stem_length,
            'price': lambda flower: flower.price
        }
        criterion = criteria[feature]
        sorted_flowers = list(sorted(self.flowers, key=criterion))
        return [flower.__str__() for flower in sorted_flowers]


wrap1 = Wrapping('Transparent', 'Glitter', 0.25)
wrap2 = Wrapping('Luxury Two-Color', 'Peach', 1.65)
wrap3 = Wrapping('Cotton Linen', 'Vintage', 3.00)
wrap4 = Wrapping('Craft Paper', 'Beige', 2.00)

ribbon1 = Ribbon('Double Face Satin', 'Golden', 1.85)
ribbon2 = Ribbon('Sheer Chiffon', 'Violet', 0.80)
ribbon3 = Ribbon('Double Face Satin', 'Rose', 1.85)

chamomile1 = Chamomile(1, "Yellow and White", 12)
violet1 = Violet(1, 'Dark Purple', 10)
rose1 = Rose(1, 'Pink', 20)
rose2 = Rose(1, 'Light Yellow', 20)
rose3 = Rose(1, 'Peach', 20)
tulip1 = Tulip(1, 'Light Pink', 15)
tulip2 = Tulip(2, 'White', 15)

bouquet1 = Bouquet(wrap4, ribbon1, rose1, rose2, rose3)
bouquet2 = Bouquet(wrap2, ribbon3, tulip2, tulip1)
bouquet3 = Bouquet(wrap3, ribbon2, chamomile1, violet1)


if __name__ == '__main__':
    print(rose1)
    # стоимость букета
    print(bouquet3.get_price)
    # сортировка цветов в букете на основе различных параметров
    print(bouquet1.sort_flowers('color'))
    # поиск цветов в букете по запросу
    print(bouquet2.search('tulip'))
    # узнать, есть ли цветок в букете
    print(chamomile1 in bouquet3)
    # определить время увядания
    print(bouquet3.wilt_time())
    # возможность получения цветка по индексу
    print(bouquet3[0])
    # пройтись по букету и получить каждый цветок по-отдельности
    for flow in bouquet1:
        print(flow)
