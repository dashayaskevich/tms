# Task 1.
def before_and_after(func):
    def wrapper(*args):
        print("Before")
        func(*args)
        print("After")
    return wrapper


# Task 2.
def decor_sum(func):
    def wrapper(*args):
        return func(*args) + 1
    return wrapper


# Task 3.
def decor_uppercase(func):
    def wrapper(arg):
        return func(arg.upper())
    return wrapper


# Task 4.
def func_name(func):
    def wrapper(*args):
        print(func.__name__)
        return func(*args)
    return wrapper


# Task 5.
def change(func):
    def wrapper(*args):
        return func(*args[::-1])
    return wrapper


# Task 6.
def timer(func):
    import time

    def wrapper(*args):
        start = time.perf_counter()
        return_value = func(*args)
        stop = time.perf_counter()
        print(f"Lead time: {stop - start}")
        return return_value
    return wrapper


# Task 7.
def typed(type='str'):
    def replace(func):
        def wrapped(*args):
            new_args = tuple()
            if type == 'int':
                for i in args:
                    if not isinstance(i, int):
                        i = float(i)
                    new_args += (i,)
            elif type == 'str':
                for i in args:
                    if not isinstance(i, str):
                        i = str(i)
                    new_args += (i,)
            return func(*new_args)
        return wrapped
    return replace
