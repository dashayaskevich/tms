import pytest
from selenium import webdriver


@pytest.fixture(scope="function", autouse=True)
def browser():
    browser = webdriver.Chrome("D:/Chromedriver/chromedriver.exe")
    yield browser
    browser.quit()
