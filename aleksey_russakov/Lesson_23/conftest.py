import pytest
from selenium import webdriver


URL = 'http://the-internet.herokuapp.com/'


@pytest.fixture()
def driver():
    driver = webdriver.Chrome("D:/Chromedriver/chromedriver.exe")
    driver.get(URL)
    yield driver
    driver.quit()
