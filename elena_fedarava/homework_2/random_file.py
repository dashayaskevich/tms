import math  # using module "math" for mathematics functions

# Task 1.1
a = float(input('Введите a '))
b = float(input('Введите b '))
# calculating and showing results
print(' Сумма:', a + b, 'Разность:', a - b, 'Произведение:', a * b)

# Task 1.2
x = int(input('Введите x'))
y = int(input('Введите y'))
# calculating and showing results
result = (math.fabs(x) - math.fabs(y)) / (1 + math.fabs(x * y))
print(result)

# Task 1.3
a = int(input('Введите длину ребра куба'))
# calculating and showing results
print('Объем куба:', a ** 3, 'Площадь боковой поверхности', a * a * 6)

# Task 1.4
a = float(input('Введите a '))
b = float(input('Введите b '))
# calculating and showing results
print(' Среднее арифметическое:', (a + b) / 2)
print('Среднее геметрическое:', math.sqrt(a * b))

# Task 1.5
a = int(input('Введите длину 1го катета'))
b = int(input('Введите длину 2го катета'))
c = (a**2 + b**2) ** 0.5  # calculating a hypotenuse
p = (a + b + c) / 2  # calculating the semi-perimeter
# calculating and showing results
print('Гипотенуза:', c)
print('Площадь треугольника:', (p * (p - a) * (p - b) * (p - c)) ** 0.5)
