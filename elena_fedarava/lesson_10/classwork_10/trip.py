# Task1: Путешествие. Вы идете в путешествие, надо подсчитать сколько у денег
# у каждого студента. Класс студен описан следующим образом:
# class Student:
#     def __init__(self, name, money):
#         self.name = name
#         self.money = money
# Необходимо понять у кого больше всего денег и вернуть его имя.
# assert most_money([ivan, oleg, sergei]) == "sergei"
# assert most_money([ivan, oleg]) == "ivan"
# assert most_money([oleg]) == "Oleg"

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

    @property
    def get_money(self):
        return self.money

    @get_money.setter
    def set_money(self, money):
        self.money = money


ivan = Student('ivan', 300)
oleg = Student('oleg', 500)
sergei = Student('sergei', 200)


def most_money(args: list):
    money_lst = [student.money for student in args]
    max_amount = max(money_lst)
    if money_lst.count(max_amount) == len(money_lst) and len(money_lst) != 1:
        return 'all'
    else:
        for i in args:
            if i.money == max_amount:
                return i.name


assert most_money([oleg]) == "oleg"
