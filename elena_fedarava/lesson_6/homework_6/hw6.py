# Task2: Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы -
# строка “c3b2a"
# Примеры для проверки работоспособности:
# "cccbba" == "c3b2a"
# "abeehhhhhccced" == "abe2h5c3ed"
# "aaabbceedd" == "a3b2ce2d2"
# "abcde" == "abcde"
# "aaabbdefffff" == "a3b2def5"


def calc_letters(str1: str) -> str:
    """Function calculates number of letters followed one by one """
    str1 = str1 + ' '  # честно, это костыль
    str2 = temp = str1[0]  # str2 = строка, хранящая список одинаковых
    # элементов, которые идут друг за другом, temp - переменная, чтобы хранить
    # пред элемент для сравнения с текущим
    result1 = ''
    for i in str1[1:]:
        if i == temp:
            str2 += i
            temp = i
        if i != temp:
            result1 = result1 + temp + str(str2.count(temp))
            str2 = i
            temp = i
    return result1.replace('1', '')


print('Task 2:', calc_letters(input('Task 2. Введите строку: ')))

# Task4: Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент с именем
# name). Также предусмотреть возможность передачи нескольких именованных и
# позиционных аргументов
#
# Функция должна возвращать следующее
# result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
# Print(result)
# {“mandatory_position_argument”: 1, “additional_position_arguments”: (2, 3),
# “mandatory_named_argument”: {“name”: “test2”}, “additional_named_arguments”:
# {“surname”: “test2”, “some”: “something”}}


def function(pos_arg, *args, name=None, **kwargs) -> dict:
    """This function is to show position and named arguments"""
    dict_name = {'name': name}
    dict_result = {'mandatory_position_argument': pos_arg,
                   'additional_position_arguments': args,
                   'mandatory_named_argument': dict_name,
                   'additional_named_arguments': kwargs}
    return dict_result


result = function(1, 2, 3, name='test', surname='test2', some='something')
print('Task 4:', result)

# Task5: На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и добавляет в него
# элементы. Функция должна вернуть измененный список. При этом исходный список
# не должен измениться. Пример c функцией которая добавляет в список символ “a”
# My_list = [1, 2, 3]
# Changed_list = change_list(My_list)
# Print(My_list)
# [1, 2, 3]
# Print(Changed_list)
# [1, 2, 3, “a”]
my_list = [1, 2, 3]


def change_list(my_lst: list) -> list:
    """This function adds a new element to the list"""
    my_lst = my_list[:]
    my_lst.append('a')
    return my_lst


changed_list = change_list(my_list)
print('Task 5:', my_list)
print('Task 5:', changed_list)

# Task6: Функция проверяющая тип данных
# Написать функцию которая принимает на фход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных
# print(my_function([1, 2, “a”, (1, 2), “b”])
# {“int”: 2, “str”: 2, “tuple”: 1}


def my_function(element: list) -> dict:
    """ The function counts number of elements with types: int, str, tuple"""
    counter_int = 0
    counter_str = 0
    counter_tuple = 0
    for i in element:
        if isinstance(i, int):
            counter_int += 1
        elif isinstance(i, str):
            counter_str += 1
        elif isinstance(i, tuple):
            counter_tuple += 1
    result_dict = {"int": counter_int, "str": counter_str,
                   "tuple": counter_tuple}
    return result_dict


print('Task 6:', my_function([1, 2, "a", (1, 2), "b"]))

# Task7: Написать пример, чтобы hash от объекта 1 и 2 были одинаковые,
# а id разные.
a = 5
b = 5
print('Task 7, Id:', id(a), id(b))
print('Task 7, Hash:', hash(a), hash(b))

# Task8: написать функцию, которая проверяет есть ли в списке объект, которые
# можно вызвать


def say_hi():
    """The function is used for verification in call"""
    return 'Hi'


def call(*args):
    """The function verifies if there is any callable element in the list"""
    lst2 = [*args]
    for i in lst2:
        if callable(i):
            print(i, 'is callable')
        else:
            print(i, 'is not callable')


call(1, say_hi, 'b')
