# Task2: Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
# заменяется другим, отстоящим от него в алфавите на фиксированное число
# позиций.
# Примеры:
# hello world! -> khoor zruog!
# this is a test string -> ymnx nx f yjxy xywnsl
# Напишите две функции - encode и decode принимающие как параметр строку и
# число
#  - сдвиг.


def encode_decode(text: str, shift: int, op: int) -> str:
    """This function encodes and decodes strings for ENG alphabet"""
    result = ''
    for c in text:
        if c.isalpha():
            if c.isupper():
                start = ord('A')
            else:
                start = ord('a')
            c_index = ord(c) - start
            if op == 1:
                new_index = (c_index + shift) % 26
            else:
                new_index = (c_index - shift) % 26
            new_unicode = new_index + start
            new_character = chr(new_unicode)
            result += new_character
        else:
            result += c
    return result


def menu():
    """This function performs input of required data"""
    print('Программа 2. Привет:', 'Выберите операцию:', '1. Encode',
          '2. Decode', sep='\n')
    n = int(input())
    if n == 1:
        text = input('Введите фразу: ')
        shift = int(input('Введите сдвиг: '))
        print(encode_decode(text, shift, n))
    elif n == 2:
        text = input('Введите фразу: ')
        shift = int(input('Введите сдвиг: '))
        print(encode_decode(text, shift, n))
    else:
        print('Нет такой операции!')
