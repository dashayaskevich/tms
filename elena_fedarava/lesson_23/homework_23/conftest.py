from selenium import webdriver
import pytest

PAGE_DYNAMIC_CONTROLS = 'http://the-internet.herokuapp.com/dynamic_controls'
PAGE_IFRAMES = 'http://the-internet.herokuapp.com/frames'


@pytest.fixture(scope='module')
def driver():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


@pytest.fixture()
def open_dynamic_controls(driver):
    driver.get(PAGE_DYNAMIC_CONTROLS)
    return driver


@pytest.fixture()
def open_iframes(driver):
    driver.get(PAGE_IFRAMES)
    return driver
