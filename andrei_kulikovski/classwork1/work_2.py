"""
1. Свяжите переменную с любой строкой, состоящей не менее чем из 8 символов.
Извлеките из строки первый символ, затем последний, третий с начала и
третий с конца. Измерьте длину вашей строки.
"""
s = 'qwertyui'
print(s[0])
print(s[-1])
print(s[2], s[-3])
print(len(s))


"""
2. Присвойте произвольную строку длиной 10-15 символов переменной и
извлеките из нее следующие срезы:
первые восемь символов
четыре символа из центра строки
символы с индексами кратными трем
переверните строку
"""
s = 'qwertyuiop'
print(s[0:8])
s1 = int(len(s) / 2)
print(s[s1:s1 + 4])
print(s[3::3])
print(s[::-1])


"""
3. Есть строка: “my name is name”. Напечатайте ее,
но вместо 2ого “name” вставьте ваше имя.
"""
# variants
msg = "my name is name"
print(msg.replace("is name", "is Andrei"))

name = "Andrei"
print(f"my name is {name}")

msg = "my name is name"
list_msg = msg.split(" ")
list_msg[3] = "Andrei"
new_msg = " ".join(list_msg)
print(new_msg)


"""
4. Есть строка: test_tring = "Hello world!", необходимо
напечатать на каком месте находится буква w
кол-во букв l
Проверить начинается ли строка с подстроки: “Hello”
Проверить заканчивается ли строка подстрокой: “qwe”
"""
test_tring = "Hello world!"
print(test_tring.index("w"))
print(test_tring.count('l'))
print(test_tring.startswith("Hello"))
print(test_tring.endswith("qwe"))
