import xml.etree.ElementTree as ET
import json
import yaml


"""
Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/заголовку/
описанию.
"""


def xml_search(arg):
    root = ET.parse('library.xml').getroot()
    for child in root:
        if (arg in child.find('author').text or
                arg in child.find('price').text or
                arg in child.find('title').text or
                arg in child.find('description').text):
            print(child.attrib, child.find('author').text,
                  child.find('price').text, child.find('title').text,
                  child.find('description').text)


xml_search('Kim')
xml_search('36.75')
xml_search('Maeve Ascendant')
xml_search('creating applications')


"""
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)
"""


file = open("students.json", "r").read()
data = json.loads(file)


def json_search(arg, kwarg):
    for student in data:
        if arg in student["Name"] or\
                arg == student["Club"] and kwarg == student["Class"]:
            print(list(filter(lambda x: x == student, data)))


json_search("Chess", "3a")
json_search("Yuki", "")
print(list(filter(lambda x: x["Gender"] == "M", data)))
print(list(filter(lambda x: x["Gender"] == "W", data)))


"""
Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json
* Создайте свой yaml файл
"""


with open('order.yaml', "r") as f:
    templates = yaml.safe_load(f)
    print(templates['invoice'])
    print(templates['bill-to']['address'])
    print(templates['product'])
with open('order.json', 'w') as f:
    yaml.dump(templates, f)

name = 'Andrei'
surname = 'Kulikovski'
age = '31'
to_yaml = {'name': name, 'surname': surname, 'age': age}
with open('my_order.yaml', "w") as f:
    yaml.dump(to_yaml, f)
