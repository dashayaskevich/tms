from abc import ABC, abstractmethod


class Pet(ABC):
    @abstractmethod
    def move(self):
        print("move")


class Parrot(Pet):
    def move(self):
        print("bbb")


par = Parrot()
par.move()


"""
Методы класса Animal:
    Идти
    Спать
    Есть
    пить
Pet:
    любить
    есть_тапки

WildAnimal:
    охотится
    рыть_яму
    рычать
"""


class Animal(ABC):
    @abstractmethod
    def move(self):
        print("move")

    @abstractmethod
    def sleep(self):
        print("sleep")

    @abstractmethod
    def eat(self):
        print("eat")

    @abstractmethod
    def drink(self):
        print("drink")


class Pet(Animal):
    @abstractmethod
    def love(self):
        print("love")

    @abstractmethod
    def eat_tap(self):
        print("eat tap")


class WildAnimal(Animal):
    @abstractmethod
    def hunt(self):
        print("hunt")

    @abstractmethod
    def scream(self):
        print("scream")


class Cat(Pet):
    def cat(self):
        print("cat")

    def move(self):
        print("i move")

    def sleep(self):
        print("i sleep")

    def eat(self):
        print("i eat")

    def drink(self):
        print("i drink")

    def love(self):
        print("i love")

    def eat_tap(self):
        print("i eat tap")


cat1 = Cat()
cat1.cat()
cat1.love()
cat1.eat_tap()
cat1.move()
cat1.sleep()
cat1.eat()
cat1.drink()


class Dog(Pet):
    def dog(self):
        print("dog")

    def move(self):
        print("i move")

    def sleep(self):
        print("i sleep")

    def eat(self):
        print("i eat")

    def drink(self):
        print("i drink")

    def love(self):
        print("i love")

    def eat_tap(self):
        print("i eat tap")


dog1 = Dog()
dog1.dog()
dog1.love()
dog1.eat_tap()
dog1.move()
dog1.sleep()
dog1.eat()
dog1.drink()


class Lion(WildAnimal):
    def lion(self):
        print("lion")

    def hunt(self):
        print("i hunt")

    def scream(self):
        print("i scream")

    def move(self):
        print("i move")

    def sleep(self):
        print("i sleep")

    def eat(self):
        print("i eat")

    def drink(self):
        print("i drink")


lion1 = Lion()
lion1.lion()
lion1.hunt()
lion1.scream()
lion1.move()
lion1.sleep()
lion1.eat()
lion1.drink()


class Wolf(WildAnimal):
    def wolf(self):
        print("wolf")

    def hunt(self):
        print("i hunt")

    def scream(self):
        print("i scream")

    def move(self):
        print("i move")

    def sleep(self):
        print("i sleep")

    def eat(self):
        print("i eat")

    def drink(self):
        print("i drink")


wol1 = Wolf()
wol1.wolf()
wol1.hunt()
wol1.scream()
wol1.move()
wol1.sleep()
wol1.eat()
wol1.drink()

"""
Создать класс Book. Атрибуты: количество страниц, год издания, автор, цена.
Добавить валидацию в конструкторе на ввод корректных данных.
Создать иерархию ошибок.
"""
# количество страниц > 4000
# год < 1980
# автор не должен содержать других символов кроме букв
# цена не должна быть меньше 100 и больше 10000


class PageNumberError(Exception):
    message = "Page number shouldn't be more than 4000"

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = "Year shouldn't be over 1980"

    def __init__(self):
        super().__init__(self.message)


class AuthorError(Exception):
    message = "Author name should contain letters only"

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = "Price should be between 100 and 1000"

    def __init__(self):
        super().__init__(self.message)


class Book:

    def __init__(self, page_num, year, author, price):
        self.page_num = self.page_num_validation(page_num)
        self.year = self.year_validation(year)
        self.author = self.author_validation(author)
        self.price = self.price_validation(price)

    @staticmethod
    def page_num_validation(page_num):
        if page_num > 4000:
            raise PageNumberError
        return page_num

    @staticmethod
    def year_validation(year):
        if year < 1980:
            raise PageNumberError
        return YearError

    @staticmethod
    def author_validation(author):
        if author.isalpha():
            return author
        raise AuthorError

    @staticmethod
    def price_validation(price):
        if price >= 1000 and price <= 100:
            raise PriceError
        return price


book_1 = Book(550, 1980, "aaa", 5030)


"""
Создать lambda функцию, которая принимает на вход имя и выводит его в формате
“Hello, {name}”
"""


# func = lambda nam: f"Hello, {name}"
# print(func("Egor"))


"""
Создать lambda функцию, которая принимает на вход список имен и выводит их
в формате “Hello, {name}” в другой список
"""


name = ["Egor", "Mark"]

# func = lambda nam: [f"{x}" for x in name]
# print(func(name))

func = map(lambda y: f"{y}", name)
print(list(func))


"""
Дан список чисел. Вернуть список, где каждый число переведено в строку
[5, 3] -> [‘5’, ‘3’]
"""
x = [5, 3]
print(list(map(lambda k: f"{k}", x)))


"""
Дан список имен, отфильтровать все имена, где есть буква k
"""
filter_name = list(filter(lambda i: "k" in i, name))
print(filter_name)


"""
отсортировать словарь с помощью функции sorted()
"""
s = {"a": 10, "b": 20, "c": 0, "d": -1}
print({k: v for k, v in sorted(s.items(), key=lambda item: item[1])})
