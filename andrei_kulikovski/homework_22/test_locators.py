import pytest
from selenium.webdriver.common.by import By


URL = "https://www.saucedemo.com/"
USER_NAME = '[id="user-name"]'
PASSWORD = '[id="password"]'
LOGIN = '[id="login-button"]'
SORT_SELECT_AZ =\
    '//select[@class="product_sort_container"]/option[@value="az"]'
SORT_SELECT_ZA =\
    '//select[@class="product_sort_container"]/option[@value="za"]'
SORT_SELECT_LO =\
    '//select[@class="product_sort_container"]/option[@value="lohi"]'
SORT_SELECT_HI =\
    '//select[@class="product_sort_container"]/option[@value="hilo"]'
SHOP_CARD = '//a[@class="shopping_cart_link"]'
TWITTER_LINK = "//a[@href='https://twitter.com/saucelabs']"
FACEBOOK_LINK = "//a[@href='https://www.facebook.com/saucelabs']"
LINKEDIN_LINK = "//a[@href='https://www.linkedin.com/company/sauce-labs/']"
SORT_MENU = "//select[@class='product_sort_container']"


@pytest.mark.parametrize('element', (1, 2, 3, 4, 5, 6))
def test_info_locators(driver, link, login, element):
    name = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({element})"
        f" .inventory_item_name")
    price = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({element})"
        f" .inventory_item_price")
    assert name.is_displayed
    assert price.is_displayed
    print(name.text, price.text)


@pytest.mark.parametrize('element', (1, 2, 3, 4, 5, 6))
def test_button_and_img_locators(driver, link, login, element):
    button_add_remove = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({element}) .btn")
    img = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({element})"
        f" .inventory_item_img")
    assert button_add_remove.is_displayed
    assert img.is_displayed


@pytest.mark.parametrize('element',
                         [TWITTER_LINK, FACEBOOK_LINK, LINKEDIN_LINK,
                          SORT_MENU, SORT_SELECT_AZ, SORT_SELECT_ZA,
                          SORT_SELECT_HI, SORT_SELECT_LO])
def test_other_locators(driver, link, login, element):
    assert driver.find_element(By.XPATH, element).is_displayed
