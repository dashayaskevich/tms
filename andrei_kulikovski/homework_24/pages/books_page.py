from pages.base_page import BasePage
from pages.books_page_locators import BooksPageLocators
from pages.book_details import BookDetailPage


class BooksPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/" \
          "en-gb/catalogue/category/books_2/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_book_sub(self):
        return self.driver.find_element(*BooksPageLocators.BOOKS_PAGE_SUB)

    def get_all_books(self):
        return self.driver.find_elements(*BooksPageLocators.BOOK_ITEMS)

    def open_book(self, title):
        for book in self.get_all_books():
            if book.find_element(*BooksPageLocators.BOOK_TITLE).text == title:
                book.find_element(*BooksPageLocators.BOOK_TITLE).click()
                return BookDetailPage(self.driver, self.driver.current_url)
