import pytest
from pages.fiction_page import FictionPage
from pages.main_page import MainPage
from selenium.common.exceptions import ElementNotInteractableException


def test_fiction_page(driver):
    page = FictionPage(driver)
    page.open_page()
    assert page.get_fiction_sub().text == "Fiction"
    assert driver.title == "Fiction | Oscar - Sandbox"
    assert "/fiction_3/" in driver.current_url


@pytest.mark.xfail(raises=ElementNotInteractableException)
def test_fiction_click(driver):
    main = MainPage(driver)
    main.open_page()
    click = FictionPage(driver)
    click.fiction_click()
    click.get_fiction_sub()
    assert driver.title == "Fiction | Oscar - Sandbox",\
        'does not open the page from the main'
