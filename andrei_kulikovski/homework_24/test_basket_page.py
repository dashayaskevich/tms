from pages.basket_page import BasketPage
from pages.main_page import MainPage


def test_basket_page(driver):
    page = BasketPage(driver)
    page.open_page()
    assert page.get_basket_sub().text == "Basket"
    assert driver.title == "Basket | Oscar - Sandbox"
    assert "/basket/" in driver.current_url


def test_basket_button(driver):
    main = MainPage(driver)
    main.open_page()
    button = BasketPage(driver)
    button.open_basket_button()
    button.basket_inner()
    assert button.basket_inner().text == "Your basket is empty." \
                                         " Continue shopping"
