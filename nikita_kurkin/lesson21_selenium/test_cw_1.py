from selenium.webdriver.common.by import By


def test_open_page(browser):
    browser.get('https://the-internet.herokuapp.com/')
    browser.find_element(By.LINK_TEXT, 'Inputs').click()
    browser.find_element(By.TAG_NAME, 'input').send_keys("123")
    title_page = browser.find_element(By.TAG_NAME, 'h3')
    # print(browser.find_element(By.XPATH, '//input[@type="number"]/div').text)
    # input_text = browser.find_element(By.XPATH,
    # '//input[@type="number"]/div/div/div')
    assert title_page.text == "Inputs"
    # assert input_text.text == '123'


def test_amazon_page(browser):
    browser.get('https://www.amazon.com')
    browser.find_element(By.XPATH, '//*[@data-csa-c-content-id="nav_cs_gb"]')\
        .click()
    browser.implicitly_wait(5)  # неявное ожидание
    locator = 'a[href="https://www.amazon.com/stores/page/preview?isSlp=1&is' \
              'Preview=1&asins=B09G9KFNC7%2CB09G91HCT8%2CB09G94CPJG%2CB0882G' \
              '76YD%2CB094JG1ZVX%2CB094JGR77Y%2CB09G5NL147%2CB09G5F418Q%2CB0' \
              '9G52YXBN%2CB09CDHZ1DD%2CB094BX4J4L%2CB08GPKC3TW%2C&ref=dlx_de' \
              'als_gd_dcl_tlt_2_d63fc7ac_dt_sl15_53"]'
    elem = browser.find_element(By.CSS_SELECTOR, locator)
    elem.click()
    browser.find_element(By.CLASS_NAME, "navFooterBackToTopText").click()


def test_positive_heroku_login(browser):
    browser.get('https://the-internet.herokuapp.com/login')
    browser.find_element(By.XPATH, '//input[@id="username"]')\
        .send_keys("tomsmith")
    browser.find_element(By.ID, 'password').send_keys("SuperSecretPassword!")
    browser.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()
    success_login = browser.find_element(By.XPATH,
                                         '//div[@class="flash success"]')
    assert success_login.text == "You logged into a secure area!\n×",\
        "check your login and password"


def test_negative_heroku_login(browser):
    browser.get('https://the-internet.herokuapp.com/login')
    browser.find_element(By.XPATH, '//input[@id="username"]')\
        .send_keys("tomsmith1")
    browser.find_element(By.ID, 'password').send_keys("SuperSecretPassword!")
    browser.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()
    failed_login = browser.find_element(By.XPATH,
                                        '//div[@class="flash error"]')
    assert failed_login.text == "Your username is invalid!\n×", "smth wrong"
