import random
import math

'''
Вставлять всё задание и далее выполнять его построчно не самая
юзабельная идея. В связи с этим выполнил задание в
формате "задание - реализация"
'''

# 1.Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
url = 'www.my_site.com#about'
print(url.replace('#', '/'))


# 2.В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
name_surname = 'Ivanou Ivan'
name_list = name_surname.split()
print(name_list[1] + ' ' + name_list[0])


# 3.Напишите программу которая удаляет пробел в начале строки
random_string = ' hi, my name is '
print(random_string.lstrip())


# 4.Напишите программу которая удаляет пробел в конце строки
print(random_string.rstrip())

# 5.a = 10, b=23, поменять значения местами, чтобы в переменную
# “a” было записано значение “23”, в “b” - значение “-10”
a = 10
b = 23
a, b = b, a
print(a, b)


# 6.значение переменной “a” увеличить в 3 раза, а
# значение “b” уменьшить на 3
a *= 3
b -= 3
print(a, b)


# 7.преобразовать значение “a” из целочисленного в
# число с плавающей точкой (float), а значение в переменной “b”
# в строку
a = float(a)
b = str(b)
print(type(a), type(b))


# 8.Разделить значение в переменной “a” на 11 и вывести результат
# с точностью 3 знака после запятой
a = int(a) / 11
print(round(a, 3))


# 9.Преобразовать значение переменной “b” в число с плавающей точкой
# и записать в переменную “c”. Возвести полученное число в 3-ю степень.
c = float(b) ** 3
print(c)


# 10.Получить случайное число, кратное 3-м
random_digit = random.randrange(0, 100, 3)
print(random_digit)


# 11.Получить квадратный корень из 100 и возвести в 4 степень
digit_sqrt = math.sqrt(100) ** 4
print(digit_sqrt)


# 12.Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
some_string = 'Hi guys'
new_some_string = some_string * 3 + "Today"
print(new_some_string)


# 13.Получить длину строки из предыдущего задания
print(len(new_some_string))


# 14.Взять предыдущую строку и вывести слово “Today” в прямом и
# обратном порядке
today_word = new_some_string[len(some_string) * 3:len(new_some_string)]
print(today_word + ' ' + today_word[::-1])


# 15.“Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в
# прямом и обратном порядке
every_third_letter = new_some_string[::3]
print(every_third_letter)
print(every_third_letter[::-1])


# 16.Используя форматирования подставить результаты из задания
# 10 и 11 в следующую строку:
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>,
# <в обратном>”
new_format_random_digit = str(random_digit)
new_format_digit_sqrt = str(digit_sqrt)
print(f'Task 10: {new_format_random_digit}, {new_format_random_digit[::-1]}'
      f' Task 11: {new_format_digit_sqrt}, {new_format_digit_sqrt[::-1]}')


# 17.Есть строка: “my name is name”. Напечатайте ее, но вместо
# 2ого “name” вставьте ваше имя.
random_string_3 = "my name is name"
rr = random_string_3.split()
rr[3] = 'Nikita'
new_string = ' '.join(rr)
print(new_string, end='\n\n')


# 18.Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print(new_some_string.capitalize())
print(new_some_string.lower())
print(new_some_string.upper())


# 19.Посчитать сколько раз слово “Task” встречается
# в строке из задания 12
print(new_some_string.find('Task'))
