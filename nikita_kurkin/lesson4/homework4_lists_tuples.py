from collections import Counter


"""
1. Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" => ["I", "love", "arrays", "they",
"are", "my", "favorite"]
"""
some_str = "Robin Singh"

"""
2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
"""
some_list = ["Ivan", "Ivanou"]
city = "Minsk"
country = "Belarus"
print(f'Привет, {some_list[0]} {some_list[1]}! Добро пожаловать'
      f' в {city} {country}')

"""
3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"
"""
some_list_1 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(' '.join(some_list_1))

"""
4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6
"""
random_list = [i for i in range(10)]
random_list.insert(2, 11)
random_list.remove(random_list[5])
print(random_list)

"""
5. Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}
1. Создайте словарь, который будет содержать в себе все элементы обоих
словарей
2. Обновите словарь “a” элементами из словаря “b”
3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
4. Проверить что есть хотя бы одно пустое значение (результат выполнения
должен быть True)
5. Отсортировать словарь по алфавиту в обратном порядке
6. Изменить значение под одним из ключей и вывести все значения
"""

a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}
c = {**a, **b}
a = {**b}

print(a.values() != '' or a.values() != 0)

for i in a.values():
    if i == '':
        print(True)

b = list(a)
b.sort(reverse=True)
print(b)

a['e'] = 11
print(a)

"""
6.Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
a. Вывести только уникальные значения и сохранить их в отдельную
переменную
b. Добавить в полученный объект значение 22
c. Сделать list_a неизменяемым
d. Измерить его длинну
"""

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
list_b = [x for i, x in enumerate(list_a) if i == list_a.index(x)]
print(list_b)
list_b.append(22)
list_a = tuple(list_a)
print(len(list_a))

"""
Задачи на закрепление форматирования:
1) Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования
"""
a = 10
b = 25
print(f'Summ is {a + b} and diff = {a - b}.')
print('Summ is', a + b, 'and diff =', a - b, end='. \n')

"""
2) Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>””
"""
list_of_children = ['Sasha', 'Vasia', 'ikalai']
print(f'First child is {list_of_children[0]}, second is {list_of_children[1]},'
      f' and last one - {list_of_children[2]}')

"""
*1) Вам передан массив чисел. Известно, что каждое число в этом массиве
имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
Напишите программу, которая будет выводить уникальное число
"""
list_of_digits = [1, 5, 2, 9, 2, 9, 1]
print(list(set(list_of_digits)))

"""
*2) Дан текст, который содержит различные английские буквы и знаки
препинания. Вам
необходимо найти самую частую букву в тексте. Результатом должна быть буква в
нижнем регистре.
При поиске самой частой буквы, регистр не имеет значения, так что при подсчете
считайте, что "A" == "a". Убедитесь, что вы не считайте знаки препинания,
цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет
буква, которая идет первой в алфавите. Для примера, "one" содержит
"o", "n", "e" по одному разу, так что мы выбираем "e".
"a-z" == "a"
"Hello World!" == "l"
"How do you do?" == "o"
"One" == "e"
"Oops!" == "o"
"AAaooo!!!!" == "a"
"a" * 9000 + "b" * 1000 == "a"
"""


def count_of_letters() -> str:
    text_str = input('Enter your string: ')
    text_str = text_str.lower().split(' ')
    text_str = ''.join(i for i in text_str if not i.isdigit())
    new_text_str = list(text_str)
    return Counter(new_text_str).most_common()[0][0]


print(count_of_letters())
