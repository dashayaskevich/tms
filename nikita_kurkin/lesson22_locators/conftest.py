import pytest
from selenium import webdriver


@pytest.fixture(scope='function', autouse=True)
def browser():
    browser = webdriver.Chrome()
    yield browser
    browser.quit()
