import pytest
import time


@pytest.fixture(scope='session', autouse=True)
def how_long_implement():
    start_time = time.perf_counter()
    yield
    end_time = time.perf_counter()
    print(f'\nTime of implementation is: {end_time - start_time}')


@pytest.fixture(scope='module', autouse=True)
def pwd_tests(request):
    yield
    print(f'\nFilename is {request.node.name}')


@pytest.fixture(scope='function', autouse=True)
def what_the_fixture_using(request, time_btw_start_and_impl_func):
    yield
    print(f'\nThis test is using next fixtures: {request.fixturenames}')
    end_implement_time = time.perf_counter()
    print(f'Time between start session and implementing'
          f' this function is: '
          f'{end_implement_time - time_btw_start_and_impl_func}')


@pytest.fixture(scope='session', autouse=True)
def time_btw_start_and_impl_func():
    start_session = time.perf_counter()
    return start_session
