# This module contains function 'bank' that has 2 args(a sum of money and
# the quantity of years that the bank will hold the money).As a result we
# get the sum of money with 10% of annual income.

# Написать функцию, которая вычисляет банковский вклад
# Пользователь делает вклад в размере a рублей сроком на years лет под 10%
# годовых (каждый год размер его вклада увеличивается на 10%.
# Эти деньги прибавляются к сумме вклада, и на них в следующем году тоже
# будут проценты).Написать функцию bank, принимающая аргументы a и years,
# и возвращающую сумму, которая будет на счету пользователя.


def bank(a: int, years: int):
    """
    This function gets two args: a-is a sum of money,years-the quantity of
    years that the bank will hold the money.
    """
    for i in range(years):
        a *= 1.1
    return round(a, 2)


def bank_input_check():
    while True:
        try:
            a = int(input('Input the sum of money: '))
            break
        except ValueError:
            print('Wrong data input. Try again')
    while True:
        try:
            years = int(input('Input the years you'
                              ' want the bank to hold your money: '))
            break
        except ValueError:
            print('Wrong data input. Try again')
    return a, years


if __name__ == '__main__':
    a, years = bank_input_check()
    print('Your final sum: ', bank(a, years))
