from functools import wraps


# Напишите декоратор, который печатает перед и после вызова функции слова
# “Before” and “After”


def dec_abc(func):
    """
    This decorator prints 'Before' before function execution and 'After'
    after function execution.
    """

    @wraps(func)
    def inner(*args):
        print('Before')
        x = func(*args)
        print('After')
        return x

    return inner


# Напишите функцию декоратор, которая добавляет 1 к заданному числу

def second_task_dec(func):
    """
    This decorator returns the value that was input as argument plus one.
    """

    @wraps(func)
    def inner(*args):
        return func(*args) + 1

    return inner


# Напишите функцию декоратор, которая переводит полученный текст в верхний
# регистр

def third_dec(func):
    """
    This decorators returns the input string using capital letters.
    """

    @wraps(func)
    def inner(arg):
        return arg.upper()

    return inner


# Напишите декоратор func_name, который выводит на экран имя функции
# (func.__name__)

def fourth_dec(arg):
    """
    This function has got decorator inside that prints the message  in
    @third_dec(), function name and arguments that were input.Returns
    the summ of arguments.
    """

    def fourth_task_dec(func):
        """
        This decorator prints the message  in @third_dec(), function
        name and arguments that were input.
        Returns the summ of arguments.
        """

        @wraps(func)
        def inner(*args):
            print(arg)
            print('func name is: ', func.__name__)
            print('args: ', *args)
            return sum(args)

        return inner

    return fourth_task_dec


# Напишите декоратор change, который делает так, что задекорированная функция
# принимает все свои не именованные аргументы в порядке, обратном тому,
# в котором их передали

def change_dec(func):
    """
    This decorator takes arguments and reverses their order.
    """

    @wraps(func)
    def inner(*args):
        return tuple(reversed(args))

    return inner


# Напишите декоратор, который вычисляет время работы декорируемой функции
# (timer)

def timer_dec(func):
    """
    This decorator calculates how long does the function execute.
    """
    import time

    @wraps(func)
    def inner(arg):
        timer = time.time()
        x = func(arg)
        print('Time of execution is: ', time.time() - timer)
        return x

    return inner


# Напишите функцию, которая вычисляет значение числа Фибоначчи для заданного
# количествa элементов последовательности и возвращает его и оберните
# ее декоратором timer и func_name

def func_name_dec(func):
    """
    This decorator prints the function name.
    """

    @wraps(func)
    def inner(arg):
        print('func name is: ', func.__name__)
        x = func(arg)
        return x

    return inner


# Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
# @typed(type=’str’)
# def add_two_symbols(a, b):
# return a + b

def check_n_add(type=None):
    """
    This decorator compare the arguments type with users choice ,converts if
    it is necessary and summarizes.
    """

    def typed(func):
        @wraps(func)
        def inner(*args):
            if type == 'int':
                k = 0
                for i in args:
                    if not isinstance(i, int):
                        i = int(i)
                        k += i
                return k
            if type == 'str':
                k = []
                for i in args:
                    if not isinstance(i, str):
                        i = str(i)
                        k.append(i)
                    else:
                        k.append(i)
                return ''.join(k)

        return inner

    return typed
