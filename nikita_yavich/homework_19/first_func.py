def islower_check(str):
    """
    Function checks if the string include only lowercase letters and
    returns True or False
    """
    return str.islower()
