import pytest
from third_func import is_special_character
from first_func import islower_check
from second_func import InputTypeError, \
    to_float_converter


@pytest.mark.repeat(3)
@pytest.mark.parametrize('value,expected_value',
                         [('abc', True), ('Abc', False),
                          ('ABC', False), ('!', False),
                          ('123', False), (' ', False),
                          (None, AttributeError),
                          (123, AttributeError)
                          ])
def test_islower_check(value, expected_value):
    if expected_value == AttributeError:
        with pytest.raises(expected_value):
            islower_check(value)
    else:
        assert islower_check(value) == expected_value


@pytest.mark.repeat(3)
@pytest.mark.parametrize('value,expected_value',
                         [(123, 123.0), ('123', 123.0),
                          (None, InputTypeError),
                          ('abc', InputTypeError),
                          ([123, 0], InputTypeError)
                          ])
def test_to_float_converter(value, expected_value):
    if expected_value == InputTypeError:
        with pytest.raises(expected_value):
            to_float_converter(value)
    else:
        assert to_float_converter(value) == expected_value


@pytest.mark.repeat(3)
@pytest.mark.parametrize('value,expected_value',
                         [('!', True), ('Abc', False),
                          (None, TypeError),
                          (123, TypeError), ('123', False),
                          (' ', False)
                          ])
def test_is_special_character(value, expected_value):
    if expected_value == TypeError:
        with pytest.raises(expected_value):
            is_special_character(value)
    else:
        assert is_special_character(value) == expected_value
