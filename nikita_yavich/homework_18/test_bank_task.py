from bank_task.bank_task import bank
import unittest


class TestBankTaskOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Preparing to start OK tests...")

    def test_bank_func_right_input(self):
        self.assertEqual(bank(1000, 2), 1210)

    def test_bank_func_zero_sum_input(self):
        self.assertEqual(bank(0, 2), 0)

    def test_bank_func_zero_years_input(self):
        self.assertEqual(bank(1000, 0), 1000)

    def test_bank_func_wrong_output(self):
        self.assertNotEqual(bank(1000, 2), 1200)

    def test_bank_func_more_arguments(self):
        with self.assertRaises(TypeError):
            bank(1000, 1000, 1000)

    def test_bank_func_less_arguments(self):
        with self.assertRaises(TypeError):
            bank(1000)

    def test_bank_func_string_sum_input(self):
        with self.assertRaises(TypeError):
            bank('1000', 2)

    def test_bank_func_string_years_input(self):
        with self.assertRaises(TypeError):
            bank(1000, '2')

    @classmethod
    def tearDownClass(cls):
        print("Finishing OK tests...")


@unittest.expectedFailure
class TestBankTaskFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Preparing to start Fail tests...")

    def test_bank_func_right_input(self):
        self.assertNotEqual(bank(1000, 2), 1210)

    def test_bank_func_wrong_output(self):
        self.assertEqual(bank(1000, 2), 1200)

    def test_bank_func_zero_sum_input(self):
        self.assertNotEqual(bank(0, 2), 0)

    def test_bank_func_zero_years_input(self):
        self.assertNotEqual(bank(1000, 0), 1000)

    @classmethod
    def tearDownClass(cls):
        print("Finishing Fail tests...")
