import requests


class Enpoints:
    PROJECTS = "projects"
    REPO_BRANCHES = "projects/{project_id}/repository/branches"
    COMMITS = "projects/{project_id}/repository/commits"
    SINGLE_COMMIT = "projects/{project_id}/repository/commits/{sha}"
    COMMENTS_OF_THE_SINGLE_COMMIT = "projects/{project_id}/" \
                                    "repository/commits/{sha}/comments"


def check_response(response):
    response.raise_for_status()
    return response


class RestHelper:
    def __init__(self, url, auth_token=None):
        self.url = url
        self.auth_token = auth_token
        self.session = requests.Session()
        if auth_token:
            self.session.headers.update(
                {"Authorization": f"Bearer {self.auth_token}"})

    def do_get(self, endpoint):
        response = self.session.get(self.url + endpoint)
        return check_response(response).json()

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response).json()


class GitLabHelper:
    def __init__(self, auth_token, api_version="v4"):
        self.auth_token = auth_token
        self.api_version = api_version
        self.url = f"https://gitlab.com/api/{self.api_version}/"
        self.session = requests.Session()
        self.session.headers.update(
            {"Authorization": f"Bearer {self.auth_token}"})

    def get_projects(self):
        return check_response(
            self.session.get(self.url + Enpoints.PROJECTS)).json()

    def get_branches(self, project_id):
        return check_response(self.session.get(
            self.url + Enpoints.REPO_BRANCHES.format(
                project_id=project_id))).json()

    def create_branch(self, project_id, branch_name, referal_branch_name):
        data = {
            "branch": branch_name,
            "ref": referal_branch_name
        }
        return check_response(self.session.post(
            self.url + Enpoints.REPO_BRANCHES.format(project_id=project_id),
            json=data)).json()


class SecondGitLabHelper(RestHelper):
    def __init__(self, auth_token, api_version="v4"):
        self.auth_token = auth_token
        self.api_version = api_version
        self.url = f"https://gitlab.com/api/{self.api_version}/"
        super().__init__(self.url, auth_token)

    def get_projects(self):
        return self.do_get(Enpoints.PROJECTS)

    def get_branches(self, project_id):
        return self.do_get(
            Enpoints.REPO_BRANCHES.format(project_id=project_id))

    def create_branch(self, project_id, branch_name, referal_branch_name):
        data = {
            "branch": branch_name,
            "ref": referal_branch_name
        }
        return self.do_post(
            Enpoints.REPO_BRANCHES.format(project_id=project_id), json=data)

    def get_commits(self, project_id):
        return self.do_get(
            Enpoints.COMMITS.format(project_id=project_id))

    def get_a_single_commit(self, project_id, sha):
        return self.do_get(
            Enpoints.SINGLE_COMMIT.format(project_id=project_id, sha=sha))

    def get_comments_of_the_commit(self, project_id, sha):
        return self.do_get(
            Enpoints.COMMENTS_OF_THE_SINGLE_COMMIT.format(
                project_id=project_id, sha=sha))

    def create_a_comment_to_the_commit(self, project_id, sha, note):
        note = {
            "note": note,

        }
        return self.do_post(
            Enpoints.COMMENTS_OF_THE_SINGLE_COMMIT.format(
                project_id=project_id, sha=sha),
            json=note)
