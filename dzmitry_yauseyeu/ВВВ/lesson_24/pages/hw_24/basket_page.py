from base_page_hw import BasePage
from basket_locators import BasketPageLocators


class BasketPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def empty_basket(self):
        page_basket = self.driver.\
            find_element(*BasketPageLocators.VIEW_BASKET)
        page_basket.click()
        self.driver.find_element(*BasketPageLocators.EMPTY_INDICATOR)
