"""1. Написать функцию, которая вычисляет банковский вклад:
Пользователь делает вклад в размере a рублей сроком на
years лет под 10% годовых (каждый год размер его вклада
увеличивается на 10%. Эти деньги прибавляются к сумме
вклада, и на них в следующем году тоже будут проценты).
Написать функцию bank, принимающая аргументы a и years,
 и возвращающую сумму, которая будет на счету пользователя.
"""


def bank(a, years):
    """the function calculates the deposit in the bank at interest"""
    for i in range(years):
        a = a + (a * 0.10)
    return(a)


result = bank(float(input("Введите сумму вклада: ")),
              int(input("На сколько лет: ")))
print(round(result, 3))

"""2. Шифр Цезаря — один из древнейших шифров.
При шифровании каждый символ заменяется другим,
 отстоящим от него в алфавите на фиксированное число позиций."""


def coder():
    """the function uses the caesar cipher encode and decode"""
    alphabet = "ABCDEFGHIJKLMNOPRSTUVWXYZabcdefghijklmnopqrstuvwxyz" \
               "ABCDEFGHIJKLMNOPRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    code_decode = input("encode - 1 or decode - 2: ")
    result = ''
    if code_decode == "1":
        message = input("message: ")
        step = int(input("step encode: "))
        for i in message:
            vol = alphabet.find(i)
            new_vol = vol + step
            if i in alphabet:
                result += alphabet[new_vol]
            else:
                result += i
        print(result)
    elif code_decode == "2":
        message = input("message: ")
        step = int(input("step decode: "))
        for i in message:
            vol = alphabet.find(i)
            new_vol = vol - step
            if i in alphabet:
                result += alphabet[new_vol]
            else:
                result += i
        print(result)


coder()
