class Employe:
    employe_count: int = 0

    def __init__(self, name, money):
        self.name = name
        self.money = money
        Employe.add_employe()

    def get_name(self):
        print(f"Имя работника {self.name}")

    @classmethod
    def add_employe(cls):
        cls.employe_count += 1

    @classmethod
    def get_amount(cls):
        print(f'Количество работников {cls.employe_count}')


em1 = Employe('Ivan', 500)
em2 = Employe('Denis', 150)
lst = [em1, em2]
for i in lst:
    i.get_name()
Employe.get_amount()


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


st1 = Student('Oleg', 200)
st2 = Student('ivan', 100)
st3 = Student('Sergey', 200)


def most_money(arg):
    money_lst = [student.money for student in arg]
    max_money = max(money_lst)
    if money_lst.count(max_money) == len(money_lst):
        return 'all'

    for student in arg:
        if student.money == max_money:
            return student.name


assert most_money([st1, st3]) == 'all'
assert most_money([st2, st3]) == 'Sergey'
