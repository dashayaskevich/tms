"""
Написать функцию, которая вычисляет банковский вклад
Пользователь делает вклад в размере a рублей сроком на years лет под 10%
годовых (каждый год размер его вклада увеличивается на 10%. Эти деньги
прибавляются к сумме вклада, и на них в следующем году тоже будут проценты).
Написать функцию bank, принимающая аргументы a и years, и возвращающую
сумму, которая будет на счету пользователя.
"""


def deposit_calculation(a: float, period: int) -> float:
    """
    this function calculates actual deposit
    """
    deposit = a
    for i in range(period):
        deposit += deposit * 0.1
    return deposit


def deposit_attributes():
    deposit = float(input("Enter sum of deposit, please: "))
    period = int(input("Enter period in years, please: "))
    result = deposit_calculation(deposit, period)
    return result


if __name__ == "__main__":
    print(deposit_attributes())
