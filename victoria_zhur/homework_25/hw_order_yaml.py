import yaml
import json

with open("order.yaml") as f:
    order = yaml.safe_load(f)

#  Напечатайте номер заказа
print("Order Number is:", order["invoice"])

#  Напечатайте адрес отправки
for address_value in order["bill-to"]["address"].items():
    print(*address_value)

# Напечатайте описание посылки, ее стоимость и кол-во
products = order["product"]
for i in products:
    print("Product description is:", i["description"],
          "\n" "Product price is:", i["price"],
          "\n" "Product quantity is:", i["quantity"])

# Сконвертируйте yaml файл в json
with open("order_to_json.json", 'w') as out_json_file:
    json.dump(order, out_json_file, default=str, indent=4)

# Создайте свой yaml файл
with open('yaml_file.yaml', "w") as out_yaml_file:
    data = {"file_type": "yaml", "file_name": "yaml_file"}
    yaml.dump(data, out_yaml_file)
