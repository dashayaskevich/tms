import string
from collections import Counter


"""1. Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" => ["I", "love", "arrays", "they", "are",
"my", "favorite"]"""

str_1 = "Robin Singh"
str_2 = "I love arrays they are my favorite"
arr_1 = str_1.split()
arr_2 = str_2.split()
print(arr_1)
print(arr_2)


"""2. Дан список: ["Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”"""

full_name = ["Ivan", "Ivanou"]
city = "Minsk"
country = "Belarus"
print(f"Привет, {full_name[0]} {full_name[1]}! Добро пожаловать в "
      f"{city} {country}.")


"""3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => 'I love arrays they are my favorite'"""

arr_3 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
arr_3_to_string = " ".join(arr_3)
print(arr_3_to_string)


"""4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6"""

arr_4 = [i for i in range(10)]
arr_4[2] = 100
del arr_4[6]
print(arr_4)


"""5.1 Есть 2 словаря a = { 'a': 1, 'b': 2, 'c': 3}, b = { 'c': 3, 'd': 4,'e':
“”}. Создайте словарь, который будет содержать в себе все элементы обоих
словарей"""

a = {"a": 1, "b": 2, "c": 3}
b = {"c": 3, "d": 4, "e": ""}
c = {**a, **b}
print(c)


"""5.2 Обновите словарь “a” элементами из словаря “b”"""

a.update(b)
print(a)


"""5.3 Проверить что все значения в словаре “a” не пустые либо не равны нулю"""

print(all(a.values()))


"""5.4 Проверить что есть хотя бы одно пустое значение (результат выполнения
должен быть True)"""

if all(a.values()) is False:
    print(True)
else:
    print("There's no any empty value in the dictionary")


"""5.5 Отсортировать словарь по алфавиту в обратном порядке"""

print(sorted(a.items(), reverse=True))


"""5.6 Изменить значение под одним из ключей и вывести все значения"""

a["a"] = 100
print(a)


"""6. Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]"""

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]


"""6.a Вывести только уникальные значения и сохранить их в отдельную
переменную"""

list_unique = list(set(list_a))
print(list_unique)


"""6.b Добавить в полученный объект значение 22"""

list_unique.append(22)
print(list_unique)


"""6.c Сделать list_a неизменяемым"""

list_a = tuple(list_a)
print(list_a)


"""6.d Измерить его длинну"""

print(len(list_a))


# Задачи на закрепление форматирования:
"""1. Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования"""

a, b = 10, 25
print("Summ is {} and diff is {}.".format(a + b, a - b))
print(f"Summ is {a + b} and diff is {a - b}.")


"""2. Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>””"""

list_of_children = ["Sasha", "Vasia", "Nikalai"]
print(f"First child is {list_of_children[0]}, second is"
      f" {list_of_children[1]}, and last one - {list_of_children[2]}")


"""Вам передан массив чисел. Известно, что каждое число в этом массиве имеет
пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
Напишите программу, которая будет выводить уникальное число"""

arr_5 = [1, 5, 2, 9, 2, 9, 1]
unique_numbers = [i for i in arr_5 if arr_5.count(i) == 1]
print(*unique_numbers)


"""Дан текст, который содержит различные английские буквы и знаки препинания.
Вам необходимо найти самую частую букву в тексте. Результатом должна быть
буква в нижнем регистре. При поиске самой частой буквы, регистр не имеет
значения, так что при подсчете считайте, что "A" == "a". Убедитесь, что вы
не считайте знаки препинания, цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой, тогда результатом
будет буква, которая идет первой в алфавите. Для примера, "one" содержит "o",
"n", "e" по одному разу, так что мы выбираем "e"."""

text = input()
text = text.lower()  # convert to lower case
text = text. replace(" ", "")  # exclude of spaces
text = "".join(i for i in text if i not in set(string.digits))  # exclude of
# digits
text = "".join(i for i in text if i not in set(string.punctuation))  # exclude
# of punctuation
text = "".join(sorted(text))  # sorting
print(Counter(text).most_common()[0][0])
