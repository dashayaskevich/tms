import pytest
import datetime


"""
Написать фикстуру, которая будет выводить время начала запуска всех тестов и
когда все тесты отработали. По итогу она должна отработать один раз!
"""


@pytest.fixture(scope="session", autouse=True)
def test_start_end_time():
    start_time = datetime.datetime.now()
    yield start_time
    end_time = datetime.datetime.now()
    print("Start time is {start}, Finish time is "
          "{finish}".format(start=start_time, finish=end_time))


"""
Написать фикстуру, которая будет выводить имя файла, из которого запущен тест.
Если тесты находятся в одном файле, для них фикстура должна отработать 1 раз!
"""


@pytest.fixture(scope="module", autouse=True)
def tests_file_name(request):
    print("File name is {name}".format(name=request.node.name))


"""
Написать фикстуру, которая для каждого теста будет выводить следующую инфу:
А) Информацию о фикстурах, которые используются в этом тесте
Б) Информацию о том, сколько времени прошло между запуском сессии и запуском
текущего теста
"""


@pytest.fixture(autouse=True)
def tests_info_and_time(test_start_end_time, request):
    now = datetime.datetime.now()
    result = now - test_start_end_time
    print("Fixtures that are used are {info}. Time between session start and "
          "launch of this test is {time}".format(info=request.fixturenames,
                                                 time=result))
