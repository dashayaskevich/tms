# IF
"""1. Если значение некой переменной больше 0, выводилось бы специальное
сообщение. Один раз выполните программу при значении переменной больше 0,
второй раз — меньше 0."""

a = int(input("Введите число: "))
if a > 0:
    print("Value is greater than 0")


"""2. Усовершенствуйте предыдущий код с помощью ветки else так, чтобы в
зависимости от значения переменной, выводилась либо 1, либо -1."""

if a > 0:
    print("1")
else:
    print("-1")


"""3. Самостоятельно придумайте программу, в которой бы использовалась
инструкция if (желательно с веткой elif). Вложенный код должен содержать не
менее трех выражений."""

b = int(input("Введите число b: "))
c = int(input("Введите число c: "))

if b >= c:
    print("b is greater than c")
elif c == 500:
    print("c is equal to 500")
elif c + b == 400:
    print("sum of b and c is equal to 400")
else:
    print("none of conditions meets the reality")


# ELIF
"""1. Напишите программу по следующему описанию: a. двум переменным
присваиваются числовые значения;"""

value_1 = int(input("Введите первое число: "))
value_2 = int(input("Введите второе число: "))


"""b. если значение первой переменной больше второй, то найти разницу
значений переменных (вычесть из первой вторую), результат связать с третьей
переменной;
c. если первая переменная имеет меньшее значение, чем вторая, то третью
переменную связать с результатом суммы значений двух первых переменных;
d. во всех остальных случаях, присвоить третьей переменной значение первой
переменной;
e. вывести значение третьей переменной на экран."""

if value_1 > value_2:
    value_3 = value_1 - value_2
elif value_1 < value_2:
    value_3 = value_1 + value_2
else:
    value_3 = value_1
print(value_3)


"""2. Придумайте программу, в которой бы использовалась инструкция
if-elif-else. Количество ветвей должно быть как минимум четыре."""

age = int(input("Введите ваш возраст: "))

if age <= 13:
    print("you are a kid")
elif 13 < age <= 17:
    print("you are a teenager")
elif 17 < age <= 40:
    print("you are young")
elif 40 < age <= 60:
    print("you are almost an old man")
else:
    print("you are definitely an old man")


# FOR
"""1. Создайте список, состоящий из четырех строк. Затем, с помощью цикла
for, выведите строки поочередно на экран."""

color = ["blue", "green", "white", "black"]
for each_string in color:
    print(each_string)


"""2. Измените предыдущую программу так, чтобы в конце каждой буквы строки
добавлялось тире. (Подсказка: цикл for может быть вложен в другой цикл.)"""

for each_string in color:
    for each_letter in each_string:
        print(each_letter + "-")


"""3. Создайте список, содержащий элементы целочисленного типа, затем с
помощью цикла перебора измените тип данных элементов на числа с плавающей
точкой. (Подсказка: используйте встроенную функцию float().)"""

integer_list = list(range(5))
for each_number in integer_list:
    each_number = float(each_number)
    print(each_number)


""""*4. Есть два списка с одинаковым количеством элементов, в каждом из них
этих списков есть элемент “hello”. В первом списке индекс этого элемента 1,
во втором 7. Одинаковых элементов в обоих списках может быть несколько.
Напишите программу которая распечатает индекс слова “hello” в первом списке и
условный номер списка, индекс слова во втором списки и условный номер списка
со словом Совпадают. Например, программа выведет на экран:
“Совпадают 1-й элемент из первого списка и 7-й элемент из второго списка”"""

list_1 = ["hey", "hello", "hi", "bye", "goodbye", "heyyy", "hola"]
list_2 = ["y", "wer", "tyu", "fgh", "gqwrwr", "werewr", "rt", "hello"]

for word in "hello":
    word_1 = list_1.index("hello")
    word_2 = list_2.index("hello")
print(f"Совпадают {word_1}-й элемент из первого списка и {word_2}-й элемент"
      f" из второго списка")


# WHILE
"""1. Напишите скрипт на языке программирования Python, выводящий ряд чисел
Фибоначчи (числовая последоватьность в которой числа начинаются с 1 и 1 или же
и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...).
Запустите его на выполнение. Затем измените код так, чтобы выводился ряд чисел
Фибоначчи, начиная с пятого члена ряда и заканчивая двадцатым."""

n = 10000
a, b = 0, 1
list_fib = []
while a < n:
    list_fib.append(a)
    a, b = b, a + b
print(list_fib[4:20])


"""2. Напишите цикл, выводящий ряд четных чисел от 0 до 20. Затем, каждое
третье число в ряде от -1 до -21."""

i = 0
while i <= 20:
    print(i)
    i += 2

z = -1
while z >= -21:
    print(z)
    z -= 3


"""3. Самостоятельно придумайте программу на Python, в которой бы
использовался цикл while."""

answer = int(input("Сколько будет 5 + 7?: "))

while answer != 12:
    print("Wrong answer")
    answer = int(input("Сколько будет 5 + 7?: "))
print("Correct answer")


# input()
"""1. Напишите программу, которая запрашивает у пользователя:
- его имя (например, "What is your name?")
- возраст ("How old are you?")
- место жительства ("Where are you live?")"""

user_name = input("What is your name?: ")
user_age = input("How old are you?: ")
user_city = input("Where are you from?: ")


"""После этого выводила бы три строки:
"This is имя"
"It is возраст"
"(S)he lives in место_жительства"""""

print(f"This is {user_name}.")
print(f"It is {user_age}.")
print(f"(S)he lives in {user_city}.")


"""2. Напишите программу, которая предлагала бы пользователю решить пример
4 * 100 - 54. Потом выводила бы на экран правильный ответ и ответ
пользователя. Подумайте, нужно ли здесь преобразовывать строку в число."""

user_result = input("Решите пример: 4 * 100 - 54 = ")
correct_result = 4 * 100 - 54
print(f"Правильный ответ - {correct_result}, Ваш ответ - {user_result}")


"""3. Запросите у пользователя четыре числа. Отдельно сложите первые два
и отдельно вторые два. Разделите первую сумму на вторую. Выведите результат на
экран так, чтобы ответ содержал две цифры после запятой."""

num_1 = float(input("Введите 1-ое число: "))
num_2 = float(input("Введите 2-ое число: "))
num_3 = float(input("Введите 3-ье число: "))
num_4 = float(input("Введите 4-ое число: "))
sum_1 = num_1 + num_2
sum_2 = num_3 + num_4
print("%.2f" % (sum_1 / sum_2))


# Additional tasks
"""1. Из заданной строки получить нужную строку:
"String" => "SSttrriinngg"
"Hello World" => "HHeelllloo  WWoorrlldd"
"1234!_ " => "11223344!!__'"""

str_1 = input("String is: ")
for each_symbol in str_1:
    print(each_symbol * 2, end="")
print()


"""2. Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
'50' == "Hot"
4 == "Nope"
"12" == "Nope"
60 == "Hot" """

x = int(input("Введите число: "))
if x ** 2 > 1000:
    print("Hot")
else:
    print("Nope")


"""3. Сложите все числа в списке, они могут быть отрицательными,
если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050"""

array_1 = []
print(sum(array_1))
array_2 = range(101)
print(sum(array_2))


"""4. Подсчет букв
Дано строка и буква => "fzizbbbu","b", нужно подсчитать сколько раз буква b
встречается в заданной строке 3
"Hello there", "e" == 3
"Hello there", "t" == 1
"Hello there", "h" == 2
"Hello there", "L" == 2
"Hello there", " " == 1"""

string_4 = input("String is: ")
char_to_search = input("Letter to search in string is: ").lower()
print(string_4.count(char_to_search))


"""5. Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет : и пробел
[] => []
["a", "b", "c"] => ["1: a", "2: b", "3: c"]"""

list_5 = ["a", "b", "c"]
numbered_list_5 = []
count = 1
for element in list_5:
    element = str(count) + ": " + element
    numbered_list_5.append(element)
    count += 1
print(numbered_list_5)


"""6. Напишите программу, которая по данному числу n от 1 до n выводит на
экран n флагов. Изображение одного флага имеет размер 4×4 символов. Внутри
каждого флага должен быть записан его номер — число от 1 до n."""

count_flags = int(input("Number of flags is: "))
for i in range(1, count_flags + 1):
    print("+__")
    print("|" + str(i), "/")
    print("|__\\")
    print("|")


"""7. Напишите программу. Есть 2 переменные salary и bonus. Salary - integer,
bonus - boolean. Если bonus - true, salary должна быть умножена на 10.
Если false - нет
10000, True == '$100000'
25000, True == '$250000'
10000, False == '$10000'
60000, False == '$60000'
2, True == '$20'
78, False == '$78'
67890, True == '$678900'"""

salary = int(input("Salary is: "))
bonus = True
if bonus:
    print("{salary_with_bonus}$".format(salary_with_bonus=salary * 10))
else:
    print("{salary_without_bonus}$".format(salary_without_bonus=salary))


"""8. Проверить, все ли элементы одинаковые
[1, 1, 1] == True
[1, 2, 1] == False
['a', 'a', 'a'] == True
[] == True"""

list_8 = []
print(all(x == list_8[0] for x in list_8[1:]))


"""9. Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
дано число 2, результат будет -> 3(1+2)
дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
1 == 1
8 == 36
22 == 253
100 == 5050"""

number_to_start = int(input("Введите число: "))
summ = 0
for i in range(number_to_start + 1):
    summ += i
print(summ)


"""10. Проверка строки. В данной подстроке проверить все ли буквы в строчном
регистре или нет и вернуть список не подходящих.
dogcat => [True, []]
doGCat => [False, ['G', 'C']]"""

str_10 = list("doGCat")
upper_case = []
for each_char in str_10:
    if each_char.isupper():
        upper_case.append(each_char)
    else:
        pass
if len(upper_case) > 0:
    print([False, upper_case])
else:
    print([True, upper_case])
