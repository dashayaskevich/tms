# 1 Создайте кортеж с цифрами от 0 до 9 и посчитайте сумму.
# (ожидаемый вывод суммы = 45)
tuple = (i for i in range(10))
print(sum(tuple))

# 2 Есть кортеж long_word = ('т', 'т', 'а', 'и', 'и', 'а', 'и', 'и',
# 'и', 'т', 'т', 'а', 'и', 'и', 'и', 'и', 'и', 'т', 'и') распечатайте на
# экран количество букв “т”, “и”, “а”
long_word = ('т', 'т', 'а', 'и', 'и', 'а', 'и', 'и', 'и', 'т', 'т', 'а',
             'и', 'и', 'и', 'и', 'и', 'т', 'и')
print(long_word.count('т'))
print(long_word.count('и'))
print(long_word.count('а'))

# 3 Допишите скрипт для расчета средней температуры.
# Постарайтесь посчитать количество дней на основе week_temp. Так наш
# скрипт сможет работать с данными за любой период.

week_temp = (26, 29, 34, 32, 28, 26, 23)
sum_temp = sum(week_temp)
days = len(week_temp)
mean_temp = sum_temp / days
print(int(mean_temp))
