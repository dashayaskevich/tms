import random


# 1 Вы идете в путешествие, надо подсчитать сколько у денег у каждого
# студента. Класс студен описан следующим образом:
class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

    @property
    def get_money(self):
        return self.money

    @get_money.setter
    def set_money(self, money):
        self.money = money


student_1 = Student('sergei', 10)
student_1.money = 11
student_2 = Student('oleg', 10)
student_2.money = 13
student_3 = Student('sergei', 10)
student_3.money = 13


def compare_money(arg):
    student_money = [student.money for student in arg]
    if student_money.count(max(student_money)) == len(student_money):
        print('all')
    for student in arg:
        if student.money == max(student_money):
            return student.name


assert compare_money([student_1, student_2, student_3]) == "oleg"
assert compare_money([student_1, student_2]) == "oleg"


# 4) Hero

class Soldier:

    def __init__(self, team):
        self.team = team
        self.id = id(self)

    @property
    def get_team(self):
        return self.team

    @get_team.setter
    def get_team(self, team):
        self.team = team

    def move_to_hero(self, hero):
        print(f'Id of the hero to whom you are moving is  {id(hero)}\n'
              f'Id of the soldier is {id(self)}')


class Hero:
    start_level = 1

    def __init__(self, team):
        self.team = team
        self.id = id(self)

    def level_up(self):
        self.start_level += 1
        return self.start_level


hero_1 = Hero('red')
hero_2 = Hero('blue')
red_team = []
blue_team = []


def create_teams():
    list_of_soldiers = [Soldier(random.choice(['red', 'blue'])) for i in
                        range(10)]
    for soldier in list_of_soldiers:
        if soldier.team == 'red':
            red_team.append(soldier)
        else:
            blue_team.append(soldier)
    if len(red_team) > len(blue_team):
        print('hero_1 level is ', hero_1.level_up())
    elif len(red_team) < len(blue_team):
        print('hero_2 level is ', hero_2.level_up())
    else:
        print('Teams are equal')


create_teams()
red_team[0].move_to_hero(hero_1)


class Goods:
    price = 100
    discount = 10

    def __init__(self, price, discount):
        self.price = price
        self.discount = discount

    @property
    def get_discount(self):
        return self.discount

    @get_discount.setter
    def set_discount(self, discount):
        self.discount = discount


goods = Goods(1000, 10)
print(goods.get_discount)
goods.price = 20
print(goods.get_discount)

# Унаследовать класс Food и Tools от Goods


class Food(Goods):
    def __init__(self, price, discount, shelf_life):
        super().__init__(price, discount)
        self.shelf_life = shelf_life


tomato = Food(100, 10, '10-10-2020')
print(tomato.shelf_life)


class Tools(Goods):
    def __init__(self, price, discount, warranty):
        super().__init__(price, discount)
        self.warranty = warranty


drill = Tools(1300, 25, '12-12-2021')
drill.price = 50
print(drill.get_discount)


class Banana(Goods):
    def __init__(self, price, discount, shelf_life):
        super().__init__(price, discount)
        self.shelf_life = shelf_life


class Apple(Goods):
    def __init__(self, price, discount, shelf_life):
        super().__init__(price, discount)
        self.shelf_life = shelf_life


class Cherry(Goods):
    def __init__(self, price, discount, shelf_life):
        super().__init__(price, discount)
        self.shelf_life = shelf_life

# Унаследовать класс Ham, Nail, Axe от класса Tools


class Ham(Tools):
    def __init__(self, price, discount, warranty):
        super().__init__(price, discount)
        self.warranty = warranty


class Nail(Tools):
    def __init__(self, price, discount, warranty):
        super().__init__(price, discount)
        self.warranty = warranty


class Axe(Tools):
    def __init__(self, price, discount, warranty):
        super().__init__(price, discount)
        self.warranty = warranty


# Создать класс Store и добавить в него словарь, где будет храниться
# item: price


class Store:
    price = 0
    discount = 10
    item_price = {}

    def __init__(self, *args):
        self.args = args
        self.price = self.sum_price(*args)

    @staticmethod
    def sum_price(*args):
        list_1 = []
        price = 0
        for instance in args:
            list_1.append(instance.price)
            price = sum(list_1)
        return price

    def add_item(self, *item):
        for instance in item:
            self.price = self.price + instance.price
        return self.price

    def del_item(self):
        self.item = "Not set"

    def overall_price_discount(self):
        return self.price - (self.price * 0.01 * self.discount)

    def overall_price_no_discount(self):
        return self.price


tomato_1 = Food(100, 10, '10-10-2020')
store_1 = Store(tomato_1, tomato, drill)
store_1.add_item(tomato_1, tomato, drill)
print(store_1.price)
print('overall_price_discount', store_1.overall_price_discount())
print('overall_price_discount', store_1.overall_price_no_discount())


# Реализовать классы GroceryStore и HardwareStore и унаследовать
# эти классы от Store
# Реализовать в GraceryStore проверку, что объект, который был передан в
# класс пренадлежит классу Food
class GroceryStore(Store):
    def __init__(self, *args):
        super().__init__(*args)
        if isinstance(*args, Food):
            print('ok')
        else:
            raise TypeError("instance must belong to the Food class")


food_instance = Food(100, 10, '10-10-2020')
test_1 = GroceryStore(food_instance)


# Реализовать в HardwareStore проверку, что объект, который был передан в
# класс пренадлежит классу Tools
class HardwareStore(Store):
    def __init__(self, *args):
        super().__init__(*args)
        if isinstance(*args, Tools):
            print('ok')
        else:
            raise TypeError("instance must belong to the Tools class")


tools_instance = Tools(1000, 10, 24)
test_2 = HardwareStore(tools_instance)
