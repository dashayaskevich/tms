import pytest
from selenium import webdriver

URL = 'http://the-internet.herokuapp.com/'


@pytest.fixture()
def driver():
    driver = webdriver.Chrome()
    driver.get(URL)
    return driver
