from decorators import before_after, increase_by_one, \
    convert_to_uppercase, func_name, change, time_measurement, \
    check_args_type


# 1) Напишите декоратор, который печатает перед и после вызова функции слова
# “Before” and “After”
@before_after
def print_inner_func(n):
    list_1 = [i for i in range(n)]
    return list_1


# 2 Напишите функцию декоратор, которая добавляет 1 к заданному числу
@increase_by_one
def basis(n):
    return n


# 3 Напишите функцию декоратор, которая переводит полученный текст в
# верхний регистр
@convert_to_uppercase
def return_str(str_any_case: str):
    return str_any_case


# 4 Напишите декоратор func_name, который выводит на экран имя функции \
# (func.__name__)
@func_name
def return_str_for_fourth_task(*args):
    return args


# 5 Напишите декоратор change, который делает так, что задекорированная
# функция принимает все свои не именованные аргументы в порядке, обратном
# тому, в котором их передали
@change
def args_kwargs_func(*args, **kwargs):
    return args, kwargs


# 6 Напишите декоратор, который вычисляет время работы декорируемой
# функции (timer)
@time_measurement
def func_to_measure():
    list_1 = [i**5 for i in range(100)]
    return list_1


# 7 Напишите функцию, которая вычисляет значение числа Фибоначчи для
# заданного количество элементов последовательности и возвращает его и
# оберните ее декоратором timer и func_name
@time_measurement
@func_name
def fib_func(n):
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return a


# 8 Напишите функцию, которая вычисляет сложное математическое выражение и
# оберните ее декоратором из пункта 1, 2
@before_after
@increase_by_one
def compl_math_exp(*args):
    return args[0] + args[1] / args[2] ** args[3]


# 9 Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
@check_args_type(type='int')
def send_params(*args):
    return args


while True:
    option = input('Выберите функцию, которую вы хотите запустить:\n1.'
                   ' декоратор,который печатает перед и после вызова '
                   'функции слова “Before” and “After”\n2. декоратор, '
                   'который добавляет 1 к заданному числу\n3. декорат,'
                   ' который переводит полученный текст в верхний '
                   'регистр\n4. декоратор , который выводит на экран '
                   'имя функции\n5 декоратор, который передает не '
                   'именованные в обратном порядке\n6. декоратор, '
                   'который вычисляет время работы декорируемой '
                   'функции\n7. функция, которая вычисляет значение'
                   ' числа Фибоначчи для заданного количество '
                   'элементов последовательности и возвращает его'
                   ' обернутая декораторами timer и func_name\n8. '
                   'функция, которая вычисляет сложное математическое'
                   ' выражение и оберните ее декоратором из пункта'
                   ' 1, 2\n9. декоратор, который проверял бы тип '
                   'параметров функции, конвертировал их если '
                   'надо и складывал\nВыбор: ')
    if option == '1':
        print_inner_func(4)
        break
    elif option == '2':
        print(basis(10))
        break
    elif option == '3':
        print(return_str(input('Введите текст ')))
        break
    elif option == '4':
        return_str_for_fourth_task()
        break
    elif option == '5':
        print(args_kwargs_func(1, 2, 3, k=0, b='Andrey'))
        break
    elif option == '6':
        print(func_to_measure())
        break
    elif option == '7':
        print(fib_func(6))
        break
    elif option == '8':
        compl_math_exp(1, 2, 3, 4)
        break
    elif option == '9':
        print(send_params(3, 5, 0, 0.1))
        break
    else:
        print("Введите значение от 1 до 9")
