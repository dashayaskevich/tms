from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


def test_dynamic_controls(driver):
    dynamic_controls_link_path = "//*[text()='Dynamic Controls']"
    checkbox_input_xpath = '//*[@type="checkbox"]'
    remove_button_path = '//*[@onclick="swapCheckbox()"]'
    message_xpath = '//*[@id="message"]'
    text_input_path = '//*[@type="text"]'
    enable_button_path = '//*[@onclick="swapInput()"]'
    driver.find_element(By.XPATH, dynamic_controls_link_path).click()
    driver.find_element(By.XPATH, checkbox_input_xpath)
    driver.find_element(By.XPATH, remove_button_path).click()
    waiter = WebDriverWait(driver, 10)
    waiter.until(EC.presence_of_element_located((By.XPATH, message_xpath)))
    try:
        driver.find_element(By.XPATH, checkbox_input_xpath)
    except NoSuchElementException:
        pass
    is_disabled = driver.find_element(By.XPATH, text_input_path)\
        .is_enabled()
    assert is_disabled is False
    driver.find_element(By.XPATH, enable_button_path).click()
    waiter.until(EC.presence_of_element_located((By.XPATH, message_xpath)))
    is_disabled = driver.find_element(By.XPATH, text_input_path) \
        .is_enabled()
    assert is_disabled is True


def test_iframe(driver):
    frame_path = "//*[text()='Frames']"
    iframe_path = "//*[text()='iFrame']"
    iframe_area_path = '//*[@title="Rich Text Area"]'
    iframe_text_path = '//*[@id="tinymce"]'
    driver.find_element(By.XPATH, frame_path).click()
    driver.find_element(By.XPATH, iframe_path).click()
    waiter = WebDriverWait(driver, 5)
    waiter.until(EC.presence_of_element_located((By.XPATH, iframe_area_path)))
    driver.switch_to.frame(driver.find_element(By.XPATH, iframe_area_path))
    iframe_text = driver.find_element(By.XPATH, iframe_text_path).text
    assert iframe_text == 'Your content goes here.', 'wrong location'
