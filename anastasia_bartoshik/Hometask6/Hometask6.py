# 1.Validate
# Ваша задача написать программу, принимающее число - номер кредитной карты(
# число может быть четным или не четным). И проверяющей может ли такая карта
# существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.


def card_number_check(card_number: list):
    """
    This function checks if your card is valid or no.
    """
    print(card_number_check.__doc__)  # to show documentation
    card_check = card_number
    for num in range(len(card_number)):
        if len(card_number) % 2 == 0:
            if num % 2 == 0:  # for even positions in the number of card
                card_check[num] *= 2
                if card_check[num] > 9:
                    card_check[num] -= 9
        else:
            if num % 2 == 1:  # for odd positions in the number of card
                card_check[num] *= 2
                if card_check[num] > 9:
                    card_check[num] -= 9
    if sum(card_check) % 10 != 0:  # the sum of numbers in all positions
        print('The number of your card is invalid')
    else:
        print('The number of your card is valid')


card_number = input('Input the number of your card: ')
while card_number.isdigit() is False:
    card_number = input('Input the correct number of your card: ')
card_number = [int(num) for num in card_number]
card_number_check(card_number)  # to trigger the function


# 2. Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы
# - строка “c3b2a"
# Примеры для проверки работоспособности:
# "cccbba" == "c3b2a"


def count_letters(final_str: str):
    """
    This function counts the number similar letters and after shows it
    """
    print(count_letters.__doc__)
    a = []
    for letters in final_str:
        b = letters + str(final_str.count(letters))
        if b not in a:
            a.append(b)
    a = ''.join(a)
    return a


final_str = input('Input your string of letters: ')
print(count_letters(final_str))


# 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя, какую операцию
# он хочет произвести над числами, а затем запрашивает два числа и выводит
# результат. Проверка деления на 0.


def simple_calculator(choose_op: str, num_1: int, num_2: int):
    """
    The creation of a simple document
    """
    print(simple_calculator.__doc__)

    if choose_op == '*':
        return num_2 * num_1
    if choose_op == '+':
        return num_2 + num_1
    if choose_op == '-':
        return num_1 - num_2
    if choose_op == '/':
        if num_2 == 0:
            return 'There is no dividing by zero'
        if num_1 > num_2:
            a = num_1 / num_2
            b = num_1 % num_2
            print('Quotient of two numbers: ', round(a))
            print('The remainder: ', b)

        return num_1 / num_2


while True:
    choose_op = input('Input the name of operation: ')
    if choose_op in ['*', '/', '+', '-']:  # list of operations
        break
    else:
        print('Incorrect type of operation, choose between (*, /, +, -):  ')

num_1, num_2 = map(int, input('Input two '
                              'numbers separated by space: ').split())

print(simple_calculator(choose_op, num_1, num_2))


# 4. Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один
# именованный аргумент, который по умолчанию равен None (в примере это
# аргумент с именем name). Также предусмотреть возможность передачи нескольких
# именованных и позиционных аргументов Функция должна возвращать следующее
# result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
# Print(result)
# 🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”: (2, 3),
# “mandatory_named_argument”: {“name”: “test2”}, “additional_named_arguments”:
# {“surname”: “test2”, “some”: “something”}}


def changed_function(x, *args, name=None, **kwargs):  # parameters
    """
    Function returns argument's characteristics.
    """
    print(changed_function.__doc__)  # to show documentation
    return {'mandatory_position_argument': x,
            'additional_position_arguments': args,
            'mandatory_named_argument': {'name': name},
            'additional_named_arguments': {**kwargs}
            }


result = changed_function(1, 2, 3, name='test', surname='test2',
                          some='something')  # arguments
print(result)


# 5. Работа с областями видимости
# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список
# и добавляет в него элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться. Пример c функцией которая
# добавляет в список символ “a”:


initial_list = [1, 2, 3]


def changed_list(initial_list: list):
    """
    Modification of the list without impacting initial list
    """
    print(changed_list.__doc__)

    final_list = initial_list[:]
    final_list.append('a')
    return final_list


final_list = changed_list(initial_list)
print(final_list)
print(initial_list)


# 6. Функция проверяющая тип данных
# Написать функцию которая принимает на вход список из чисел,
# строк и таплов. Функция должна вернуть сколько в списке элементов
# приведенных данных
# print(my_function([1, 2, “a”, (1, 2), “b”])
# 🡪 {“int”: 2, “str”: 2, “tuple”: 1}


def count_func_type(my_list: list):
    """
    This function takes a list and shows count of types.
    """
    print(count_func_type.__doc__)

    int_count = 0
    str_count = 0
    tuple_count = 0
    for elements in range(len(my_list)):

        if isinstance(my_list[elements], int):
            int_count += 1
        if isinstance(my_list[elements], str):
            str_count += 1
        if isinstance(my_list[elements], tuple):
            tuple_count += 1

    type_dict = {'int': int_count, 'str': str_count, 'tuple': tuple_count}
    return type_dict


my_list = [1, 2, 'a', (1, 2), 'b']
print(count_func_type(my_list))


# 7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные.


a = input('Input the first data: ')
b = input('Input the second data: ')
print('Print ID of the first data: ', id(a), 'Print ID of the second data: ',
      id(b), sep='\n')
print('Print HASH of the first data: ', hash(a),
      'Print HASH of the second data: ', hash(b), sep='\n')
print('Is a - b?', a is b)


# 8. написать функцию, которая проверяет есть ли в списке объект, которые можно
# вызвать


def check_func(my_list: list):
    """
    This function checks the list and shows another list of bool values of
    each element to be callable.
    """
    print(check_func.__doc__)

    return [callable(i) for i in my_list]


my_list = [1, 2, 3, 'corgi', 'cats', True, check_func]
print(check_func(my_list))
